package models

import scalikejdbc.specs2.mutable.AutoRollback
import org.specs2.mutable._
import scalikejdbc._


class PowerplantSpec extends Specification {

  "Powerplant" should {

    val p = Powerplant.syntax("p")

    "find by primary keys" in new AutoRollback {
      val maybeFound = Powerplant.find(1L)
      maybeFound.isDefined should beTrue
    }
    "find by where clauses" in new AutoRollback {
      val maybeFound = Powerplant.findBy(sqls.eq(p.id, 1L))
      maybeFound.isDefined should beTrue
    }
    "find all records" in new AutoRollback {
      val allResults = Powerplant.findAll()
      allResults.size should be_>(0)
    }
    "count all records" in new AutoRollback {
      val count = Powerplant.countAll()
      count should be_>(0L)
    }
    "find all by where clauses" in new AutoRollback {
      val results = Powerplant.findAllBy(sqls.eq(p.id, 1L))
      results.size should be_>(0)
    }
    "count by where clauses" in new AutoRollback {
      val count = Powerplant.countBy(sqls.eq(p.id, 1L))
      count should be_>(0L)
    }
    "create new record" in new AutoRollback {
      val created = Powerplant.create(userId = 1L, powertype = "MyString", capacity = 123)
      created should not beNull
    }
    "save a record" in new AutoRollback {
      val entity = Powerplant.findAll().head
      // TODO modify something
      val modified = entity
      val updated = Powerplant.save(modified)
      updated should not equalTo(entity)
    }
    "destroy a record" in new AutoRollback {
      val entity = Powerplant.findAll().head
      val deleted = Powerplant.destroy(entity) == 1
      deleted should beTrue
      val shouldBeNone = Powerplant.find(1L)
      shouldBeNone.isDefined should beFalse
    }
    "perform batch insert" in new AutoRollback {
      val entities = Powerplant.findAll()
      entities.foreach(e => Powerplant.destroy(e))
      val batchInserted = Powerplant.batchInsert(entities)
      batchInserted.size should be_>(0)
    }
  }

}
