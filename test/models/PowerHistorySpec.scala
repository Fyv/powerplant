package models

import scalikejdbc.specs2.mutable.AutoRollback
import org.specs2.mutable._
import scalikejdbc._
import java.time.{LocalDateTime}


class PowerHistorySpec extends Specification {

  "PowerHistory" should {

    val ph = PowerHistory.syntax("ph")

    "find by primary keys" in new AutoRollback {
      val maybeFound = PowerHistory.find(1L)
      maybeFound.isDefined should beTrue
    }
    "find by where clauses" in new AutoRollback {
      val maybeFound = PowerHistory.findBy(sqls.eq(ph.id, 1L))
      maybeFound.isDefined should beTrue
    }
    "find all records" in new AutoRollback {
      val allResults = PowerHistory.findAll()
      allResults.size should be_>(0)
    }
    "count all records" in new AutoRollback {
      val count = PowerHistory.countAll()
      count should be_>(0L)
    }
    "find all by where clauses" in new AutoRollback {
      val results = PowerHistory.findAllBy(sqls.eq(ph.id, 1L))
      results.size should be_>(0)
    }
    "count by where clauses" in new AutoRollback {
      val count = PowerHistory.countBy(sqls.eq(ph.id, 1L))
      count should be_>(0L)
    }
    "create new record" in new AutoRollback {
      val created = PowerHistory.create(powerplantId = 1L, userId = 1L, value = 123, createdDate = null)
      created should not beNull
    }
    "save a record" in new AutoRollback {
      val entity = PowerHistory.findAll().head
      // TODO modify something
      val modified = entity
      val updated = PowerHistory.save(modified)
      updated should not equalTo(entity)
    }
    "destroy a record" in new AutoRollback {
      val entity = PowerHistory.findAll().head
      val deleted = PowerHistory.destroy(entity) == 1
      deleted should beTrue
      val shouldBeNone = PowerHistory.find(1L)
      shouldBeNone.isDefined should beFalse
    }
    "perform batch insert" in new AutoRollback {
      val entities = PowerHistory.findAll()
      entities.foreach(e => PowerHistory.destroy(e))
      val batchInserted = PowerHistory.batchInsert(entities)
      batchInserted.size should be_>(0)
    }
  }

}
