name := """powerplant"""
organization := "com.fyv"

version := "1.0-SNAPSHOT"

scalaVersion := "2.12.6"

libraryDependencies ++= Seq(
  guice, jdbc, ehcache, ws

  , "mysql" % "mysql-connector-java" % "5.1.44"

  , "org.scalikejdbc" %% "scalikejdbc" % "3.2.0"
  , "org.scalikejdbc" %% "scalikejdbc-config" % "3.2.0"
  , "org.scalikejdbc" %% "scalikejdbc-play-dbapi-adapter" % "2.6.0-scalikejdbc-3.2"
  , "org.scalikejdbc" %% "scalikejdbc-streams" % "3.3.0"


  , "io.circe" %% "circe-core" % "0.9.3"
  , "io.circe" %% "circe-generic" % "0.9.3"
  , "io.circe" %% "circe-parser" % "0.9.3"
  , "io.circe" %% "circe-java8" % "0.9.3"
  , "io.circe" %% "circe-generic-extras" % "0.9.3"
  , "com.dripower" %% "play-circe" % "2609.1"

  , "com.beachape" %% "enumeratum" % "1.5.13"
  , "com.beachape" %% "enumeratum-circe" % "1.5.17"
  , "com.beachape" %% "enumeratum-play" % "1.5.13"

  , "io.kanaka" %% "play-monadic-actions" % "2.1.0"

  , "org.mindrot" % "jbcrypt" % "0.3m"

)

lazy val smartlib = ProjectRef(file("../smartlib"), "smartlib")

lazy val root = (project in file("."))
  .enablePlugins(PlayScala)
  .enablePlugins(ScalikejdbcPlugin)
