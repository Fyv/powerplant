package api.services

import java.time.LocalDateTime

import api.dto.PowerplantDto
import javax.inject.{Inject, Singleton}
import models.{PowerHistory, Powerplant, User}
import scalikejdbc.{DB, DBSession}

import scala.util.Try

@Singleton
class PowerplantService @Inject()() {

  def addPowerplant(powerplantDto: PowerplantDto, user: User): Try[Powerplant] =
    Try {
      DB localTx { implicit session =>
        val powerplant = Powerplant.create(
          userId = user.id,
          powertype = powerplantDto.powertype,
          capacity = powerplantDto.capacity
        )

        val capacity: Int = powerplant.capacity

        historize(powerplant, user, capacity, capacity)

        powerplant
      }
    }

  def consume(powerplant: Powerplant, user: User, value: Int) =
    Try {

        val rest = PowerHistory.findLastBy(powerplant, user) map { case lastHistory =>
          val rest = lastHistory.rest - value.abs
          if (rest < 0)
            throw new Exception(s"Canno't consume more than ${lastHistory.rest} energy")
          else rest
        } getOrElse (powerplant.capacity)

        historize(
          powerplant,
          user,
          value,
          rest
        )

    }

  def charge(powerplant: Powerplant, user: User, value: Int) =
    Try {

        val rest = PowerHistory.findLastBy(powerplant, user) map { case lastHistory =>
          val rest = lastHistory.rest + value.abs
          if (rest > powerplant.capacity)
            throw new Exception(s"Canno't charge more than ${powerplant.capacity} ")
          else rest
        } getOrElse (powerplant.capacity)


        historize(
          powerplant,
          user,
          value,
          rest
        )

    }


  def historize(powerplant: Powerplant, user: User, value: Int, rest: Int)(implicit session: DBSession = PowerHistory.autoSession) = {

    PowerHistory.create(
      powerplantId = powerplant.id,
      userId = user.id,
      value = value,
      rest = rest,
      createdDate = LocalDateTime.now()
    )
  }
}
