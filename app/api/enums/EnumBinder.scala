package api.enums

import java.sql.PreparedStatement

import enumeratum._
import scalikejdbc._


trait EnumBinder[A <: enumeratum.EnumEntry] extends Enum[A] with CirceEnum[A] {
  self =>

  implicit val typeBinder: TypeBinder[A] =
    TypeBinder.string.map(self.withName(_))

   implicit val optTypeBinder: TypeBinder[Option[A]] =
    TypeBinder.string.map(self.withNameOption(_))


  implicit def parameterBinderFactory[T <: A] = ParameterBinderFactory[T] {
    value =>
      (stmt: PreparedStatement, idx: Int) => {

        stmt.setString(idx, value.toString)
      }
  }

}
