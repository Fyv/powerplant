package api.enums


import scalikejdbc._
import enumeratum._


sealed trait Powertype extends EnumEntry
object Powertype extends EnumBinder[Powertype] with CirceEnum[Powertype] {
  val values = findValues

  case object eolien extends Powertype
  case object solar extends Powertype
  case object geothermal extends Powertype
  case object coal extends Powertype
  case object nuclear extends Powertype
  case object hydroelectric extends Powertype

  implicit val ageClassFormat = enumeratum.Forms.format(Powertype)
}
