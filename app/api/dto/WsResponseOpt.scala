package api.dto

import io.circe.generic.semiauto._
import io.circe.syntax._
import io.circe.{Decoder, Json, ObjectEncoder}

object WsResponseOpt  {
  implicit val jsonEncoder: ObjectEncoder[WsResponseOpt] = deriveEncoder[WsResponseOpt]
  implicit val jsonDecoder: Decoder[WsResponseOpt] = deriveDecoder[WsResponseOpt]


}

case class WsResponseOpt(
  errors: List[String] = Nil,
  status: WsResponseStatus = WsResponseStatus.Success,
  data:Option[WsResponseData] = None
) extends WsResponse {
  override def json: Json = this.asJson
}


object WsResponseList  {
  implicit val jsonEncoder: ObjectEncoder[WsResponseList] = deriveEncoder[WsResponseList]
  implicit val jsonDecoder: Decoder[WsResponseList] = deriveDecoder[WsResponseList]
}

case class WsResponseList(
  errors: List[String] = Nil,
  status: WsResponseStatus = WsResponseStatus.Success,
  data:List[WsResponseData] = List.empty
) extends WsResponse {
  override def json: Json = this.asJson
}
