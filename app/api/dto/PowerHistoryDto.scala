package api.dto

import java.time.LocalDateTime

import io.circe.generic.semiauto.{deriveDecoder, deriveEncoder}
import io.circe.syntax._
import io.circe.{Decoder, Json, ObjectEncoder}


case class PowerHistoryDto(
  value: Int,
  rest:Int,
  createdDate: LocalDateTime,
  powerplantDto: Option[PowerplantDto] = None
) extends WsResponseData {
  override def json: Json = this.asJson
}


object PowerHistoryDto extends JsonTime {

  implicit val jsonEncoder: ObjectEncoder[PowerHistoryDto] = deriveEncoder[PowerHistoryDto]
  implicit val jsonDecoder: Decoder[PowerHistoryDto] = deriveDecoder[PowerHistoryDto]

}
