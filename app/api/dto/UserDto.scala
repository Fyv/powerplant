package api.dto

import io.circe.generic.semiauto.{deriveDecoder, deriveEncoder}
import play.api.data.Form
import play.api.data.Forms._


case class UserDto(email: String, password: String)

object UserDto {

  implicit val jsonEncoder = deriveEncoder[PowerplantDto]
  implicit val jsonDecoder = deriveDecoder[PowerplantDto]

  val form = Form(
    mapping(
      "email" -> email,
      "password" -> nonEmptyText
    )(UserDto.apply _)(UserDto.unapply _)
  )

}

