package api.dto

import api.enums.Powertype
import io.circe.generic.semiauto.{deriveDecoder, deriveEncoder}
import io.circe.syntax._
import io.circe.{Decoder, Json, ObjectEncoder}
import play.api.data.Form
import play.api.data.Forms._

case class PowerplantDto(
    id: Option[Long]
  , powertype: Powertype
  , capacity: Int
  , history: List[PowerHistoryDto] = List.empty
) extends WsResponseData {
  override def json: Json = this.asJson
}

object PowerplantDto {

  implicit val jsonEncoder: ObjectEncoder[PowerplantDto] = deriveEncoder[PowerplantDto]
  implicit val jsonDecoder: Decoder[PowerplantDto] = deriveDecoder[PowerplantDto]

  val form = Form(
    mapping(
      "id" -> optional(longNumber),
      "powertype" -> of[Powertype],
      "capacity" -> number,
      "history" -> ignored(List.empty[PowerHistoryDto])
    )(PowerplantDto.apply _)(PowerplantDto.unapply _)
  )

}
