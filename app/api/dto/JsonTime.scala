package api.dto

import java.time.{LocalDate, LocalDateTime}
import java.time.format.DateTimeFormatter

import io.circe.{Decoder, Encoder}
import io.circe.java8.time.TimeInstances

trait JsonTime extends TimeInstances {

  val strlocalDateTimePattern = "dd/MM/yyyy HH:mm:ss"
  val localDateTimePattern = DateTimeFormatter.ofPattern(strlocalDateTimePattern)

  implicit final val decodeLocalDateTime: Decoder[LocalDateTime] = decodeLocalDateTime(localDateTimePattern)
  implicit final val encodeLocalDateTime: Encoder[LocalDateTime] = encodeLocalDateTime(localDateTimePattern)

  val strLocalDatePattern = "dd/MM/yyyy"
  val localDatePattern = DateTimeFormatter.ofPattern(strLocalDatePattern)
  implicit final val decodeLocalDate: Decoder[LocalDate] = decodeLocalDate(localDatePattern)
  implicit final val encodeLocalDate: Encoder[LocalDate] = encodeLocalDate(localDatePattern)
}