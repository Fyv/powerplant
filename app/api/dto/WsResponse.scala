package api.dto

import enumeratum._
import io.circe.syntax._
import io.circe.{Json, _}

sealed trait WsResponseStatus extends EnumEntry

object WsResponseStatus extends Enum[WsResponseStatus] with CirceEnum[WsResponseStatus] {
  val values = findValues

  case object Success extends WsResponseStatus
  case object Error extends WsResponseStatus

}


trait WsResponse {
  val errors: List[String]
  val status: WsResponseStatus

  def json: Json
}

object WsResponseData {

  type WsIntType = Int

  implicit val decoder: Decoder[WsResponseData] =
    Decoder[PowerplantDto].map[WsResponseData](identity)
    .or(Decoder[PowerHistoryDto].map[WsResponseData](identity))
    .or(Decoder[PowerRepartitionDto].map[WsResponseData](identity))


  implicit val encoder: Encoder[WsResponseData] = Encoder.instance {
    case dto: PowerplantDto => dto.asJson
    case dto: PowerHistoryDto => dto.asJson
    case dto: PowerRepartitionDto=> dto.asJson
  }

}


trait WsResponseData {
  def json: Json
}


