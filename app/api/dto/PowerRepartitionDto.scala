package api.dto


import api.enums.Powertype
import io.circe.generic.semiauto.{deriveDecoder, deriveEncoder}
import io.circe.parser._
import io.circe.syntax._
import io.circe.{Decoder, Encoder, Json, Printer}

case class PowerRepartitionDto(
  powertype: Option[Powertype] = None,
  consumption: Int, production: Int
) extends WsResponseData {
  override def json: Json = this.asJson
}


object PowerRepartitionDto {

  val printer = Printer.noSpaces.copy(dropNullValues = true)
  implicit val jsonEncoder: Encoder[PowerRepartitionDto] = deriveEncoder[PowerRepartitionDto].mapJson { json =>
    parse(json.pretty(printer)).toOption getOrElse (json)
  }
  implicit val jsonDecoder: Decoder[PowerRepartitionDto] = deriveDecoder[PowerRepartitionDto]

}
