package controllers

import akka.NotUsed
import akka.stream.Materializer
import akka.util.Timeout
import api.dto._
import api.services.PowerplantService
import io.circe.Json
import io.circe.syntax._
import io.kanaka.monadic.dsl._
import javax.inject._
import models.{PowerHistory, Powerplant, User}
import play.api.libs.{Comet, EventSource}
import play.api.mvc._
import scalikejdbc.DB
import scalikejdbc.streams.{DatabasePublisher, _}

import scala.concurrent.ExecutionContext
import scala.concurrent.duration._
import scala.util.Try

/**
  * This controller creates an `Action` to handle HTTP requests to the
  * application's home page.
  */
@Singleton
class PowerplantCtrl @Inject()(
  cc: ControllerComponents,
  powerplantService: PowerplantService,
  userAction: UserAction,
)(implicit ec: ExecutionContext, materializer:Materializer) extends AbstractController(cc) with SmartCtrl {

  def add() = userAction.async { implicit request =>

    implicit val wsResponse = new WsResponseOpt()
    val form = PowerplantDto.form.bindFromRequest

    for {
      powerplantdto <- form ?| (err => handleError(err.errorsAsJson.toString()))
      powerplant <- powerplantService.addPowerplant(powerplantdto, request.user) ?| (err => handleError(err.getMessage))
    } yield {

      Ok(wsResponse.copy(
        data = Some(
          powerplantdto.copy(
            id = Some(powerplant.id)
          )
        )
      ).asJson)
    }
  }

  def charge(powerId: Long, value: Int) = userAction.async { implicit request =>

    implicit val wsResponse = new WsResponseOpt()

    executeHistorize(
      powerId,
      value,
      (v: Int) => v > 0,
      "value must be positive",
      powerplantService.charge _
    ) map { history =>

      Ok(wsResponse.copy(
        data = Some(
          history
        )
      ).asJson)
    }
  }

  def consume(powerId: Long, value: Int) = userAction.async { implicit request =>

    implicit val wsResponse = new WsResponseOpt()

    executeHistorize(
      powerId,
      value,
      (v: Int) => v < 0,
      "value must be negative",
      powerplantService.consume _
    ) map { history: PowerHistoryDto =>

      Ok(wsResponse.copy(
        data = Some(
          history
        )
      ).asJson)
    }
  }

  private def executeHistorize(powerId: Long, value: Int, checkValue: Int => Boolean, checkValueMsg: String, callService: (Powerplant, User, Int) => Try[PowerHistory])
    (implicit wsResponse: WsResponseOpt, request: UserRequest[AnyContent]) = {

    for {
      powerplant <- Powerplant.find(powerId) ?| handleError("No powerplant found for id")
      _ <- (powerplant.userId == request.user.id) ?| handleError("User is not allowed")
      _ <- checkValue(value) ?| handleError(checkValueMsg)
      history <- callService(powerplant, request.user, value) ?| (err => handleError("" + err))
    } yield {

      PowerHistoryDto(
        powerplantDto = Some(PowerplantDto(
          id = Some(powerplant.id),
          powertype = powerplant.powertype,
          capacity = powerplant.capacity
        )),
        value = history.value,
        rest = history.rest,
        createdDate = history.createdDate
      )

    }
  }

  def remainingEnergy = userAction { implicit request =>

    implicit val wsResponse = new WsResponseList()

    val powerplants = Powerplant.findByUser(request.user)

    val powerWithConsumption = powerplants zip (powerplants map {
      PowerHistory.findByPowerplant(_)
    })

    val self = request.host + "" + request.uri

    val data = powerWithConsumption map { case (powerplant, histories) =>
      PowerplantDto(
        id = Some(powerplant.id),
        powertype = powerplant.powertype,
        capacity = powerplant.capacity,
        history = histories.map { history =>
          PowerHistoryDto(
            value = history.value,
            rest = history.rest,
            createdDate = history.createdDate
          )
        }
      )
    }

    Ok(
      wsResponse.copy(
        data = data
      ).asJson
    )
  }

  def repartitionEnergy = userAction { implicit request =>

    val repartition = PowerHistory.findRepartitionByUserGroupByNature(request.user) map { case (powertype, consumption, production) =>
      PowerRepartitionDto(Some(powertype), consumption, production)
    }

    Ok(WsResponseList(data = repartition).asJson)
  }

  def balance = userAction { implicit request =>

    val repartition = PowerHistory.findRepartitionByUser(request.user) map { case (consumption, production) =>
      PowerRepartitionDto(
        consumption = consumption,
        production = production
      )
    }

    Ok(WsResponseList(data = repartition).asJson)
  }

  import akka.stream.scaladsl.Source
  import play.api.http.ContentTypes
  import akka.stream.Materializer
  import akka.stream.scaladsl.Source
  import play.api.http.ContentTypes
  import play.api.libs.Comet
  import play.api.libs.json._
  import play.api.mvc._

  def stream = userAction { implicit request =>

    implicit val timeout: Timeout = Timeout(15 seconds)


    val publisher: DatabasePublisher[PowerHistory] = DB readOnlyStream {
      PowerHistory.streamBy(request.user)
    }

    val source: Source[String, NotUsed] = Source.fromPublisher(publisher).map(history => {
      PowerHistoryDto(
        value = history.value,
        rest = history.rest,
        createdDate = history.createdDate
      ).asJson.toString()
    })

    Ok.chunked(source via EventSource.flow).as(ContentTypes.EVENT_STREAM)
  }


  def demo = Action { implicit request =>

    Ok(views.html.index())
  }


}
