package controllers

import api.dto.{UserDto, WsResponseOpt}
import io.circe.syntax._
import io.kanaka.monadic.dsl._
import javax.inject._
import models.User
import org.mindrot.jbcrypt.BCrypt
import play.api.mvc._

import scala.concurrent.ExecutionContext
import scala.util.{Failure, Success, Try}

@Singleton
class UserCtrl @Inject()(
  cc: ControllerComponents
)(implicit ec: ExecutionContext) extends AbstractController(cc) with SmartCtrl {

  def register() = Action.async { implicit request =>

    implicit val wsResponse = new WsResponseOpt()

    val form = UserDto.form.bindFromRequest()

    for {
      userDto <- form ?| (formWithError => handleError(formWithError.errorsAsJson.toString()))
    } yield {
      val password = BCrypt.hashpw(userDto.password, BCrypt.gensalt())

      Try {
        User.create(
          userDto.email,
          password
        )
      } match {
        case Failure(f) => {
          handleError(f.getMessage)
        }
        case Success(u) => {
          Ok(wsResponse.asJson)
        }
      }

    }

  }


}
