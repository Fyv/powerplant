package controllers

import api.dto.{WsResponseOpt, WsResponseStatus}
import javax.inject.Inject
import models.User
import org.mindrot.jbcrypt.BCrypt
import play.api.mvc.Results._
import play.api.mvc._

import scala.concurrent.{ExecutionContext, Future}

class UserRequest[A](val user: User, request: Request[A]) extends WrappedRequest[A](request)

class UserAction @Inject()(val parser: BodyParsers.Default)(implicit val executionContext: ExecutionContext)
  extends ActionBuilder[UserRequest, AnyContent] {

  override def invokeBlock[A](request: Request[A], block: (UserRequest[A]) => Future[Result]): Future[Result] = {

    val authUserKey = "Auth-user"
    val maybeUsername: Option[String] = request.headers.get(authUserKey).orElse{
      request.queryString.get(authUserKey).flatMap(_.headOption)
    }

    val authPassKey = "Auth-pass"
    val maybeAuthPass = request.headers.get(authPassKey).orElse{
      request.queryString.get(authPassKey).flatMap(_.headOption)
    }

    (maybeUsername, maybeAuthPass) match {
      case (Some(userName), Some(authPass)) => {

        User.findByEmail(userName) map {
          case user if BCrypt.checkpw(authPass, user.password)=> {
            block(new UserRequest(user, request))
          }
          case _ => notAuth
        } getOrElse notAuth
      }
      case _ => notAuth
    }
  }

  lazy val notAuth = Future.successful(Unauthorized)
}



