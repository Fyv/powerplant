package controllers

import api.dto.{WsResponseOpt, WsResponseStatus}
import io.circe.syntax._
import play.api.i18n.I18nSupport
import play.api.libs.circe.Circe
import play.api.libs.json.Writes
import play.api.mvc._


trait SmartCtrl extends BaseController with Circe with I18nSupport {

  def handleError(error: String)(implicit wsResponse: WsResponseOpt) = handleErrors(List(error))

  def handleErrors(errors: List[String])(implicit wsResponse: WsResponseOpt) = {

    val errorRes = wsResponse.copy(
      errors = errors,
      status = WsResponseStatus.Error
    )
    BadRequest(errorRes.asJson)
  }
}