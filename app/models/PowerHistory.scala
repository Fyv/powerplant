package models

import java.time.LocalDateTime

import api.enums.Powertype
import scalikejdbc._

case class PowerHistory(
  id: Long,
  powerplantId: Long,
  userId: Long,
  value: Int,
  rest:Int,
  createdDate: LocalDateTime) {

  def save()(implicit session: DBSession = PowerHistory.autoSession): PowerHistory = PowerHistory.save(this)(session)

  def destroy()(implicit session: DBSession = PowerHistory.autoSession): Int = PowerHistory.destroy(this)(session)

}


object PowerHistory extends SQLSyntaxSupport[PowerHistory] {

  override val tableName = "power_history"

  override val columns = Seq("id", "powerplant_id", "user_id", "value", "rest", "created_date")

  def apply(ph: SyntaxProvider[PowerHistory])(rs: WrappedResultSet): PowerHistory = apply(ph.resultName)(rs)
  def apply(ph: ResultName[PowerHistory])(rs: WrappedResultSet): PowerHistory = new PowerHistory(
    id = rs.get(ph.id),
    powerplantId = rs.get(ph.powerplantId),
    userId = rs.get(ph.userId),
    value = rs.get(ph.value),
    rest = rs.get(ph.rest),
    createdDate = rs.get(ph.createdDate)
  )

  val ph = PowerHistory.syntax("ph")

  override val autoSession = AutoSession

  def find(id: Long)(implicit session: DBSession = autoSession): Option[PowerHistory] = {
    withSQL {
      select.from(PowerHistory as ph).where.eq(ph.id, id)
    }.map(PowerHistory(ph.resultName)).single.apply()
  }

  def findAll()(implicit session: DBSession = autoSession): List[PowerHistory] = {
    withSQL(select.from(PowerHistory as ph)).map(PowerHistory(ph.resultName)).list.apply()
  }

  def countAll()(implicit session: DBSession = autoSession): Long = {
    withSQL(select(sqls.count).from(PowerHistory as ph)).map(rs => rs.long(1)).single.apply().get
  }

  def findBy(where: SQLSyntax)(implicit session: DBSession = autoSession): Option[PowerHistory] = {
    withSQL {
      select.from(PowerHistory as ph).where.append(where)
    }.map(PowerHistory(ph.resultName)).single.apply()
  }

  def findAllBy(where: SQLSyntax)(implicit session: DBSession = autoSession): List[PowerHistory] = {
    withSQL {
      select.from(PowerHistory as ph).where.append(where)
    }.map(PowerHistory(ph.resultName)).list.apply()
  }

  def countBy(where: SQLSyntax)(implicit session: DBSession = autoSession): Long = {
    withSQL {
      select(sqls.count).from(PowerHistory as ph).where.append(where)
    }.map(_.long(1)).single.apply().get
  }

  def create(
    powerplantId: Long,
    userId: Long,
    value: Int,
    rest: Int,
    createdDate: LocalDateTime)(implicit session: DBSession = autoSession): PowerHistory = {
    val generatedKey = withSQL {
      insert.into(PowerHistory).namedValues(
        column.powerplantId -> powerplantId,
        column.userId -> userId,
        column.value -> value,
        column.rest -> rest,
        column.createdDate -> createdDate
      )
    }.updateAndReturnGeneratedKey.apply()

    PowerHistory(
      id = generatedKey,
      powerplantId = powerplantId,
      userId = userId,
      value = value,
      rest = rest,
      createdDate = createdDate)
  }

  def batchInsert(entities: Seq[PowerHistory])(implicit session: DBSession = autoSession): List[Int] = {
    val params: Seq[Seq[(Symbol, Any)]] = entities.map(entity =>
      Seq(
        'powerplantId -> entity.powerplantId,
        'userId -> entity.userId,
        'value -> entity.value,
        'rest -> entity.rest,
        'createdDate -> entity.createdDate))
    SQL("""insert into power_history(
      powerplant_id,
      user_id,
      value,
      rest,
      created_date
    ) values (
      {powerplantId},
      {userId},
      {value},
      {rest},
      {createdDate}
    )""").batchByName(params: _*).apply[List]()
  }

  def save(entity: PowerHistory)(implicit session: DBSession = autoSession): PowerHistory = {
    withSQL {
      update(PowerHistory).set(
        column.id -> entity.id,
        column.powerplantId -> entity.powerplantId,
        column.userId -> entity.userId,
        column.value -> entity.value,
        column.rest -> entity.rest,
        column.createdDate -> entity.createdDate
      ).where.eq(column.id, entity.id)
    }.update.apply()
    entity
  }

  def destroy(entity: PowerHistory)(implicit session: DBSession = autoSession): Int = {
    withSQL { delete.from(PowerHistory).where.eq(column.id, entity.id) }.update.apply()
  }

  def findLastBy(powerplant: Powerplant, user: User) = findBy(
    sqls.eq(ph.powerplantId, powerplant.id)
      .and.eq(ph.userId, user.id)
      .orderBy(ph.createdDate).desc
      .limit(1)
  )

  def findByPowerplant(powerplant: Powerplant): List[PowerHistory] = findAllBy(
    sqls.eq(ph.powerplantId, powerplant.id)
      .orderBy(ph.createdDate).asc
  )

  val p = Powerplant.p
  val u = User.u

  def findRepartitionByUserGroupByNature(user:User)(implicit session: DBSession = autoSession) = {

    sql"""
      SELECT
      p.`powertype`,
      SUM(CASE WHEN ph.value<0 THEN ph.value ELSE 0 END) as neg,
      SUM(CASE WHEN ph.value>=0 THEN ph.value ELSE 0 END) as pos
      FROM power_history ph
      JOIN powerplant p ON p.`id` = ph.`powerplant_id`
      JOIN user u ON u.id = ph.`user_id`
      WHERE u.id = ${user.id}
      GROUP BY p.powertype
     """.map { x =>
      (x.get[Powertype](1), x.get[Int](2), x.get[Int](3))
    }.list().apply()
  }

  def findRepartitionByUser(user:User)(implicit session: DBSession = autoSession) = {

    sql"""
      SELECT
      SUM(CASE WHEN ph.value<0 THEN ph.value ELSE 0 END) as neg,
      SUM(CASE WHEN ph.value>=0 THEN ph.value ELSE 0 END) as pos
      FROM power_history ph
      JOIN powerplant p ON p.`id` = ph.`powerplant_id`
      JOIN user u ON u.id = ph.`user_id`

      WHERE u.id = ${user.id}
     """.map { x =>
      (x.get[Option[Int]](1), x.get[Option[Int]](2))
    }.list().apply().map{ case (neg, pos) => (neg.getOrElse(0) -> pos.getOrElse(0)) }
  }


  import scalikejdbc.streams._

  def streamBy(user: User): StreamReadySQL[PowerHistory] = {
    withSQL {
      select
        .from(PowerHistory as ph)
        .join(Powerplant as p).on(p.id, ph.powerplantId)
        .join(User as u).on(u.id, ph.userId)
        .where.eq(u.id, user.id)
    }.map(PowerHistory(ph.resultName)).iterator()
  }

}
