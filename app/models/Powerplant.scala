package models

import api.enums.Powertype
import scalikejdbc._

case class Powerplant(
  id: Long,
  userId: Long,
  powertype: Powertype,
  capacity: Int) {

  def save()(implicit session: DBSession = Powerplant.autoSession): Powerplant = Powerplant.save(this)(session)

  def destroy()(implicit session: DBSession = Powerplant.autoSession): Int = Powerplant.destroy(this)(session)

}


object Powerplant extends SQLSyntaxSupport[Powerplant] {

  override val tableName = "powerplant"

  override val columns = Seq("id", "user_id", "powertype", "capacity")

  def apply(p: SyntaxProvider[Powerplant])(rs: WrappedResultSet): Powerplant = apply(p.resultName)(rs)
  def apply(p: ResultName[Powerplant])(rs: WrappedResultSet): Powerplant = new Powerplant(
    id = rs.get(p.id),
    userId = rs.get(p.userId),
    powertype = rs.get(p.powertype),
    capacity = rs.get(p.capacity)
  )

  val p = Powerplant.syntax("p")

  override val autoSession = AutoSession

  def find(id: Long)(implicit session: DBSession = autoSession): Option[Powerplant] = {
    withSQL {
      select.from(Powerplant as p).where.eq(p.id, id)
    }.map(Powerplant(p.resultName)).single.apply()
  }

  def findAll()(implicit session: DBSession = autoSession): List[Powerplant] = {
    withSQL(select.from(Powerplant as p)).map(Powerplant(p.resultName)).list.apply()
  }

  def countAll()(implicit session: DBSession = autoSession): Long = {
    withSQL(select(sqls.count).from(Powerplant as p)).map(rs => rs.long(1)).single.apply().get
  }

  def findBy(where: SQLSyntax)(implicit session: DBSession = autoSession): Option[Powerplant] = {
    withSQL {
      select.from(Powerplant as p).where.append(where)
    }.map(Powerplant(p.resultName)).single.apply()
  }

  def findAllBy(where: SQLSyntax)(implicit session: DBSession = autoSession): List[Powerplant] = {
    withSQL {
      select.from(Powerplant as p).where.append(where)
    }.map(Powerplant(p.resultName)).list.apply()
  }

  def countBy(where: SQLSyntax)(implicit session: DBSession = autoSession): Long = {
    withSQL {
      select(sqls.count).from(Powerplant as p).where.append(where)
    }.map(_.long(1)).single.apply().get
  }

  def create(
    userId: Long,
    powertype: Powertype,
    capacity: Int)(implicit session: DBSession = autoSession): Powerplant = {
    val generatedKey = withSQL {
      insert.into(Powerplant).namedValues(
        column.userId -> userId,
        column.powertype -> powertype,
        column.capacity -> capacity
      )
    }.updateAndReturnGeneratedKey.apply()

    Powerplant(
      id = generatedKey,
      userId = userId,
      powertype = powertype,
      capacity = capacity)
  }

  def batchInsert(entities: Seq[Powerplant])(implicit session: DBSession = autoSession): List[Int] = {
    val params: Seq[Seq[(Symbol, Any)]] = entities.map(entity =>
      Seq(
        'userId -> entity.userId,
        'powertype -> entity.powertype,
        'capacity -> entity.capacity))
    SQL("""insert into powerplant(
      user_id,
      powertype,
      capacity
    ) values (
      {userId},
      {powertype},
      {capacity}
    )""").batchByName(params: _*).apply[List]()
  }

  def save(entity: Powerplant)(implicit session: DBSession = autoSession): Powerplant = {
    withSQL {
      update(Powerplant).set(
        column.id -> entity.id,
        column.userId -> entity.userId,
        column.powertype -> entity.powertype,
        column.capacity -> entity.capacity
      ).where.eq(column.id, entity.id)
    }.update.apply()
    entity
  }

  def destroy(entity: Powerplant)(implicit session: DBSession = autoSession): Int = {
    withSQL { delete.from(Powerplant).where.eq(column.id, entity.id) }.update.apply()
  }


  def findByUser(user: User):List[Powerplant] = findAllBy(
    sqls.eq(
      p.userId, user.id
    )
  )


}
