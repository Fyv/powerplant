const es = new EventSource("/stream?Auth-user=fyv3@gmail.com&Auth-pass=hello");

let events = []

const listener = function (event) {
	const div = document.createElement("div");

	if (events.indexOf(event.data) < 0) {
		events.push(event.data)
		div.appendChild(document.createTextNode(event.data))
	}

	document.body.appendChild(div);
};

const error = function(event) {
	const div = document.createElement("div");
	div.appendChild("Erreur")
	document.body.appendChild(div);
}

es.onmessage = listener;
es.onerror = error;
